const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/* Schema for email and Query */
const UserSchema = new Schema({

    userEmail:{
        type:Schema.Types.String
    },
    userQuery:[{
        type: Schema.Types.String
    }]

});

const User = mongoose.model('User', UserSchema);

module.exports = {User}