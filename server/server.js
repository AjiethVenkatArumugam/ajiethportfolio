/* Importing mongoose and express */
const path = require('path');
const express = require('express');
const mongoose = require('mongoose');

/*Importing Schema */
const { User } = require('./models/user')

const app = express();

const publicPath = path.join(__dirname, '..', 'build');

const port = process.env.PORT || 5000;

const connection = mongoose.connection;

app.use(express.json());

/* Connecting to the mongoose atlas */
mongoose.connect('mongodb+srv://Ajieth:Ajiethdb@ajiethportfolio.w8mwg.mongodb.net/test'||'mongodb://localhost/portfolio', {useNewUrlParser: true});

connection.on('error', () => console.error('connection error:'));
connection.once('open', () => console.log('connection successful:'));

/* Get function from the react application */
app.get('/portfolio', async (request,response) => {

    const  user = await User.find({ });
    response.send(user);

});

app.use(express.static(publicPath));

/* Get function from the react application to integrate with react application for heroku by calling index.html */
app.get('*', (req, res) => {
    res.sendFile(path.join(publicPath, 'index.html'));
});

/* Post function from the react application */
app.post('/portfolio', async (request,response) => {

    const  { userEmail,userQuery } = request.body;

    /* Checking for duplication of the email */
    const check = await User.find({userEmail}).countDocuments() > 0
    if (check == true){
        const  user = await User.updateOne({ userEmail} ,{$push: {userQuery} });
        response.send(user);
    }else{
        const  user = await User.create({ userEmail,userQuery});
        response.send(user);
    }
});


app.listen(port, () => {
    console.log('Hello World I run on PORT ' + port);
});