import React from "react";
import {Typography} from "@material-ui/core";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { BottomNavigation } from '@material-ui/core';

/* Custom Hook to make styles */
const useStyles = makeStyles({

    /* Footer background Color */
    pageFooter: {

        marginTop:'3rem',
        background: '#121212',

    },

    /* Bottom Footer Text */
    bottomText: {

        padding:'1rem',

        color:'#ffffff',
    }
});

/* Footer component function */
export function Footer(){

    const classes = useStyles();

    return(

        <BottomNavigation className={classes.pageFooter}>

            <Typography className={classes.bottomText}>

                Copyright 2020

            </Typography>

        </BottomNavigation>


    )
}