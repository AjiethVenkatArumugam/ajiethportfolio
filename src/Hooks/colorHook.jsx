import React from "react";
import {useEffect} from 'react';

/* Color Custom Hook for the background Color of the pages */
export function useColorHooks() {
    useEffect(() => {
        document.body.style.backgroundColor = '#EEEEEE'
    })
}