import React from "react"
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import AppBar from "@material-ui/core/AppBar";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { useHistory } from "react-router-dom";
import Typography from "@material-ui/core/Typography";

/* Custom Hook to make styles */
const useStyles = makeStyles({

    /* navigationContainer */
    navigationBar: {

        background: '#212121',
        width:'100%',

    },

    /* displaying the navigationBar as flex Containers */
    displayNav:{

        display:'flex',
        flex:1,
        justifyContent:'space-between',

    },

    /* Title of the Navigation Bar */
    titleElement: {

        flex:2,
        fontSize:"150%",

        color:"#FFFFFF",
        fontWeight:"bold",

    },

    /* Button  container for the navigation Bar */
    buttonContainer: {

        flex:1,

        display: 'flex',

    },

    /* Color of the Button in Navigation Bar */
    buttonColor: {


        color:'#FFFFFF',
        fontWeight:"bold",
        paddingLeft:"3rem",

    },
});

/* Navigation Bar component function */
export function Navigation(){

    const history = useHistory();

    const classes = useStyles();

    /* History of the HomePage URL which is shown url tab */
    function homeNavigation(){

        history.push('/');
    }

    /* History of the ContactPage URL which is shown url tab */
    function contactNavigation(){

        history.push('/contact')
    }

    return(

        <AppBar className={classes.navigationBar}>

            <Toolbar >

                <div className={classes.displayNav}>

                    <Typography className={classes.titleElement}>

                        PORTFOLIO

                    </Typography>

                    <Typography className={classes.buttonContainer} >

                        <Button className={classes.buttonColor}
                                variant={"text"}
                                onClick={homeNavigation}>

                            Home
                        </Button>

                        <Button className={classes.buttonColor}
                                variant={"text"}
                                onClick={contactNavigation}>

                            Contact
                        </Button>

                    </Typography>

                </div>

            </Toolbar>

        </AppBar>
    )
}