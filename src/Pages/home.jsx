import React from "react"
import makeStyles from "@material-ui/core/styles/makeStyles";
import {Typography} from "@material-ui/core";
import Fade from 'react-reveal/Fade';
import Flip from 'react-reveal/Flip';
import 'react-animated-slider/build/horizontal.css';
import AwesomeSlider from 'react-awesome-slider';
import 'react-awesome-slider/dist/styles.css';
import 'react-awesome-slider/dist/custom-animations/cube-animation.css';
import Container_4_Image from '../Assets/Images/Container-4.jpg';

/*https://github.com/rcaferati/react-awesome-slider (Animation Component referred from this github)*/

/* Custom Hook to make styles */
const useStyles = makeStyles({

    /* Home page to determine the total height & width property */
    homePageContainer: {

        width:"100%",
        height:"10%",

    },

    /* Background Container for first and third Slide of the HomePage */
    firstAndThirdSlideContainer:{

        marginTop:"10%",

        position:'relative',

    },

    /* Image for the first and third slide in Home page */
    image: {

        width:"100%",
        height:'100%',

    },

    /* Text block for the first slide in HomePage */
    firstSlideTextBlock:{

        position: 'absolute',

        top: '40%',
        left: '35%',

        fontFamily: 'Poppins,sans-serif',
        fontWeight:"bold",

        transform: 'translate(-50%, -50%)',

        color: '#424242',

        paddingLeft: '20px',
        paddingRight: '20rem',

    },

    /* Second Slide Container of the HomePage */
    secondSlideContainer:{

        display:'flex',

        marginTop:"-0.25rem"
    },

    /* Text for the second slide left side container */
    secondSlideLeftTextContainer:{

        flex:2,

        padding:'1rem',
        paddingTop:'12rem',

        textAlign:"center",

        color:'#424242',
        backgroundColor:'#eff0f2',

        fontFamily: 'Poppins,sans-serif',

    },

    /* Title for the second Slide in the textContainer of the Home Page */
    secondSlideTitleContainer:{

        textAlign:'center',

        fontWeight:"bold",

        marginBottom:'2rem',
    },

    /* Image for the second Slide container */
    secondSlideImageContainer: {

        height: '100vh',

        flex: 1
    },

    /* Text for the second slide right side container*/
    secondSlideRightTextContainer:{

        flex:2,

        padding:'1rem',
        paddingTop:'8rem',

        alignItems:'center',
        justifyContent:'center',
        textAlign:"center",

        color:'#FFFFFF',
        backgroundColor:'#030406',

        fontFamily: 'Poppins,sans-serif',
    },

    /* Text for the third slide container*/
    thirdSlideTextContainer:{

        position: 'absolute',
        top: '20%',
        left: '13%',
        right:'51%',

        fontSize:'3rem',
        fontFamily: 'Nanum Pen Script, cursive',

        color: '#37474F',

        paddingLeft: '20px',
        paddingRight: '20px'
    },

    /* Link Button in the homepage */
    linkButton: {

        height:"2rem",

        fontWeight:"bold",
        fontSize:"2rem",
        fontFamily: 'Nanum Pen Script, cursive',

        transform:'rotate(350deg)',
    },

    /* Text for the fourth slide container*/
    fourthSlideTextContainer:{

        position: 'absolute',
        top: '5%',
        left: '50%',

        fontSize:'4rem',
        fontFamily: 'Nanum Pen Script, cursive',

        color: '#FFFFFF',

        paddingLeft: '20px',
        paddingRight: '20px'
    },

    fourthSlideText:{
        position: 'absolute',
        top: '20%',
        left: '35%',
        padding:'2rem',

        fontSize:'2.5rem',
        fontFamily: 'Nanum Pen Script, cursive',

        color: '#FFFFFF',
    }
});

/* Home page component function */
export function HomePage(){

    const classes = useStyles();

    return(

        <div className={ classes.homePageContainer}>

            <AwesomeSlider animation="cubeAnimation" fillParent={true} bullets={false}>

                <div className={classes.firstAndThirdSlideContainer}>

                    <img className={classes.image} alt={"First_Container"} src={"https://images.pexels.com/photos/4157621/pexels-photo-4157621.jpeg?cs=srgb&dl=pexels-abhilash-sahoo-4157621.jpg&fm=jpg"}/>


                    <div className={classes.firstSlideTextBlock}>

                        <Fade top big cascade  >

                            <h3 style={{fontWeight:"900", fontSize:"3rem"}}> AJIETHVENKAT ARUMUGAM </h3>

                        </Fade>


                        <Typography style={{fontSize:"1rem"}}>

                            <Fade top big cascade>

                                    Hello, Welcome to my Portfolio Website. I am Excited to share myself to you,Lets get going

                            </Fade>
                            <Fade bottom big cascade>

                            <ul>

                                        <li>
                                            My name is Ajieth Venkat (aka AJ among my friends). I am born and bought up from India.
                                            I did my school and undergrad in my hometown (Coimbatore).
                                        </li>

                                        <li>
                                            After finishing my studies I got into a exciting working field as an iOS Application Developer.
                                            Later I found myself attracted to various new tech so I took a big step in coming to United States of America.
                                        </li>

                                        <li>
                                            I started my adventure in <a href={"https://www.newhaven.edu/"} title={"Visit my University"} target="_blank" rel={"noreferrer"}>
                                            University of New Haven.</a> where I graduated with a major in Master's in Computer Science.
                                            While I studied I did some cool projects in Android, iOS, React (FullStack). This helped me to drove more deep into new technologies.
                                        </li>

                                        <li>
                                            After my studies Is started mey career as an Intern in Infinite Options as React Developer. I collaborated
                                            with more than 5 person in a team to help them assist in development.
                                        </li>

                                        <li>
                                            After full-filling my internship I joined a company to work as an AOSP developer which consists of working in Android, React, React-Native.
                                            Here I am still learning new tech and building my skills and producing a more reliable product with the knowledge I have inherited through my life.
                                        </li>

                                    </ul>
                                    {/*He is pursuing his Master's major in Computer Science.*/}
                                    {/*He has done several projects in both mobile and Web applications. He have worked in both*/}
                                    {/*Android and iOS in mobile and now currently learning React Native through Expo.*/}
                                    {/*He also worked in this portfolio under the React framework environment with hosting a express*/}
                                    {/*server and Mongo database. He will be graduating in December 2020 will flying colors.*/}

                                    </Fade>

                        </Typography>

                    </div>

                </div>

                <div className={classes.secondSlideContainer}>

                    <Typography className={classes.secondSlideLeftTextContainer}>


                        <Typography className={classes.secondSlideTitleContainer} variant={'h4'}>

                            <Fade left big cascade >

                                AREA OF INTEREST

                            </Fade>

                        </Typography >

                        <Typography >

                            <Fade left big cascade>

                                <ul style={{textAlign:"center"}} >
                                    <p>&#9672; Interested in application development and would like to work in a agile environment.</p>
                                    <p>&#9672; Would like to in a fast paced environment.</p>
                                    <p>&#9672; Want to learn more about latest technologies of the organization.</p>
                                    <p>&#9672; Projects done in Android ,iOS and Web Development.</p>
                                </ul>

                            </Fade>

                        </Typography>



                        <h3 style={{fontWeight:"bold"}}> Would like to work as </h3>

                        <Typography style={{textAlign:"left", marginLeft:"9rem"}}>

                            <Fade left big cascade >

                                <li style={{fontWeight:"bold"}}> Mobile Developer </li>
                                <li style={{fontWeight:"bold"}}> Web Developer </li>
                                <li style={{fontWeight:"bold"}}> Software Developer </li>

                            </Fade>

                        </Typography>

                    </Typography>

                    <img alt={"Second_Container"} src={"https://images.pexels.com/photos/3541916/pexels-photo-3541916.jpeg?cs=srgb&dl=pexels-matheus-viana-3541916.jpg&fm=jpg"} className={classes.secondSlideImageContainer}/>

                    <Typography className={classes.secondSlideRightTextContainer}>

                        <Typography className={classes.secondSlideTitleContainer} variant={'h4'}>

                            <Fade right big cascade >

                                TECHNOLOGIES

                            </Fade>

                        </Typography >

                        <Fade right big cascade >

                            <h3> Languages </h3>
                            <p> C, C++, Swift, Kotlin, JAVA, JavaScript, HTML, CSS, Python &
                                React, React-Native, Node.js.
                            </p>

                            <h3> Cloud Platforms </h3>
                            <p> Firebase, Heroku, MongoDB Atlas</p>

                            <h3> Hands on Experience </h3>
                            <p> AOSP, FullStack(MERN) and Mobile(Android &iOS) </p>

                            <h3> Version Control</h3>
                            <p> GitHub, Bitbucket, Redmine, GitLab</p>

                            <h3> Android Technologies </h3>
                            <p> Bluetooth, Wifi, SDK, Libraries </p>

                        </Fade>

                    </Typography>

                </div>

                <div className={classes.firstAndThirdSlideContainer}>

                    <img alt={"Third_Container"} src={"https://images.pexels.com/photos/2034373/pexels-photo-2034373.jpeg?cs=srgb&dl=pexels-prr-2034373.jpg&fm=jpg"} className={classes.image}/>

                    <div className={classes.thirdSlideTextContainer}>

                        <Fade top big cascade>

                            <p style={{transform:'rotate(350deg)',fontWeight:"bold"}}> My Projects in GitHub </p>

                            {/*<p style={{transform:'rotate(350deg)', fontSize:"2rem"}}> Visit my Github </p>*/}

                        </Fade>

                        <div className={classes.linkButton}>

                            <a href={'https://github.com/UNH-Android-Fall19/final-project-AjiethVenkatUNH.git'} target="_blank" rel={"noreferrer"} >

                                <Fade top big cascade>

                                    ChatBuzz - Android

                                </Fade>

                            </a>

                            <Fade top big cascade>

                            <p style={{fontSize:"1.5rem"}}>  This is a Android Chat app which was developed in Kotlin </p>

                            </Fade>

                            <a href={'https://github.com/UNH-Android-Fall19/final-project-AjiethVenkatUNH.git'} target="_blank" rel={"noreferrer"}>

                                <Fade top big cascade>

                                    GoMovie - Android

                                </Fade>

                            </a>

                            <p style={{fontSize:"1.5rem"}}>

                                <Fade top big cascade>

                                This is a Android Ticket Booking app which was developed
                                in JAVA

                                </Fade>

                            </p>


                        </div>

                    </div>

                </div>

                <div>

                    <img className={classes.image} src={Container_4_Image} alt={"Fourth_Container"} />

                    <div className={classes.fourthSlideTextContainer}>

                        <Flip top cascade>

                        <p> Experience </p>

                        </Flip>

                    </div>

                    <div className={classes.fourthSlideText}>

                        <Flip top cascade>

                        <p> * 1 year experience in developing iOS Apps using Swift</p>
                        <p> * 2 years experience in javascript in react framework developing web application for all screen sizes </p>
                        <p> * 2 years experience in creating, developing and deploying android apps in playStore </p>
                        <p> * Currently working on Android Open Source Project to develop and maintain custom devices with android OS </p>

                        </Flip>

                    </div>

                    {/*<div style={{left:"75%"}} className={classes.fourthSlideText}>*/}

                    {/*    <Flip top cascade>*/}

                    {/*    <p> * IOS </p>*/}
                    {/*    <p> * React Native </p>*/}
                    {/*    <p> * JAVA </p>*/}
                    {/*    <p> * Video Game Design</p>*/}

                    {/*    </Flip>*/}

                    {/*</div>*/}

                </div>

            </AwesomeSlider>

        </div>
    )
}

