import React from "react";
import {Typography} from "@material-ui/core";
import Button from '@material-ui/core/Button';
import makeStyles from "@material-ui/core/styles/makeStyles";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Axios from "axios";


/* Custom Hook to make styles */
const useStyles = makeStyles({

    /* Contact page to determine the total height & width */
    contactPageContainer: {

        minHeight: '88vh',
        paddingTop: '28px',

    },

    /* Contact Info in the contactPage */
    contactInfoContainer:{

        marginTop:'5%',
        display:'flex',
        justifyContent:'space-evenly',

        alignItems:'center'

    },

    /* Contact Text in contactPage */
    contactText:{

        color:'#000000',

    },

    iconImage:{

        margin:"1rem",

        width:"3rem",
        height:"3em",

    },

    /* Image for the contactPage */
    imageContainer: {

        width: '100%',
        height: '45vh',

    },

    /* Input Element for the user to send the query*/
    emailInputElement: {

        height:"1rem",

        background:'#1a150f',
        color:'#F5F5F5',

    },

    /* Input element for the Query */
    queryInputElement: {

        height:"10rem",

        background:'#1a150f',
        color:'#b0bec5',

    },

    /* Button in the contact Page */
    sendButtonElement: {

        marginLeft:'55%',
        marginTop:'1rem',

        width: '10%',
        height: '10%',

        background:'#212121',
        color:'#ffffff'

    },

});

/* Contact page component function */
export function Contact(){

    const [open, setOpen] = React.useState(false);

    const [employees, setData] = React.useState([])

    React.useEffect(() => {

        (async () => {

            const result = await Axios.get("/portfolio")
            const email = result.data;
            setData(email)

        })();
    },[]);

    const classes = useStyles();

    /* Handle Function to check if the message box is open or closed */
    const handleClickOpen = () => {

        setOpen(true);

        const userEmailId = document.getElementById("email").value
        const userQueryData = document.getElementById("query").value

        console.log(userEmailId , userQueryData)

        const newPost = {
            userEmail:userEmailId,
            userQuery:userQueryData
        };

        const sendPostRequest = async () => {

            try {

                const response = await Axios.post("/portfolio", newPost);
                console.log(response)

            }catch (err){

                console.error(err);
            }
        };

        sendPostRequest(); 
    };

    const handleClose = () => {

        setOpen(false);

        window.location.reload()
    };



    return(
        <div className={classes.contactPageContainer}>

            <img className={classes.imageContainer} src={" https://images.pexels.com/photos/16516/pexels-photo.jpg?cs=srgb&dl=pexels-khairul-nizam-16516.jpg&fm=jpg"} alt={"Contacts_Image"} />

            <div className={classes.contactInfoContainer}>

                <Typography className={classes.contactText}>

                    <h3> Contact </h3>
                    AjiethVenkat Arumugam<br/>
                    Email: <a href={"mailto:AjiethVenkat@hotmail.com"}> AjiethVenkat@hotmail.com </a><br/>
                    Phone: +1 (475) - 439 -9864<br/>

                    <a href={"https://www.linkedin.com/in/ajieth-venkat-358072109/"} target={"_blank"} rel={"noreferrer"}>

                        <img className={classes.iconImage} src={"https://cdn1.iconfinder.com/data/icons/social-media-circle-7/512/Circled_Linkedin_svg-512.png"} alt={"LinkedIn"}/>

                    </a>

                    <a href={"https://www.facebook.com/ajieth.venkat/"} target={"_blank"} rel={"noreferrer"}>

                        <img className={classes.iconImage} src={"https://cdn3.iconfinder.com/data/icons/social-media-black-white-2/512/BW_Facebook_glyph_svg-512.png"} alt={"Facebook"}/>

                    </a>

                </Typography>

                <div className={classes.contactText}>

                    <h3> Email </h3>

                    <textarea id={"email"} placeholder="Email" className={classes.emailInputElement} cols={50} rows={10}/>

                    <div>

                        <h3> Query </h3>

                        <textarea id={"query"} placeholder="Write anything you want to say about me!! :)" className={classes.queryInputElement} cols={50} rows={10}/>

                    </div>

                </div>

            </div>

            <Button variant={"contained"}  onClick={handleClickOpen} className={classes.sendButtonElement}>

                SUBMIT

            </Button>

            <Dialog open={open} onClose={handleClose} aria-labelledby={"form-dialog-title"}>

                <DialogTitle id="form-dialog-title"> THANK YOU </DialogTitle>

                <DialogContent>

                    <DialogContentText>

                        The query has been submitted. Will contact you ASAP!

                    </DialogContentText>

                </DialogContent>

                <DialogActions>

                    <Button  onClick={handleClose} color="primary">

                        Close

                    </Button>

                </DialogActions>

            </Dialog>

            <div>

            </div>

        </div>
    )
}