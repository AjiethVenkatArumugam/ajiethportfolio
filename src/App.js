import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";

/* Importing the custom pages as each components */
import { Navigation } from "./Navigation/navigation";
import { HomePage } from "./Pages/home";
import { useColorHooks } from "./Hooks/colorHook";
import { Footer} from "./Footer/footer";
import { Contact} from "./Pages/contact";

/* Main function for all the pages and elements */
export default function App() {

    useColorHooks()

    return (

        <Router>

            <div>

                <Link to="/">Home</Link>

                <Link to="/contact">About</Link>

                <Navigation/>

                <Switch>

                    <Route path="/contact">

                        <Contact />

                    </Route>

                    <Route path="/">

                        <HomePage />

                    </Route>

                </Switch>

                <Footer/>

            </div>

        </Router>
    );
}




